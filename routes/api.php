<?php
use Illuminate\Support\Facades\Route;
use Orgo\DuplicateField\Http\Controllers\DuplicateController;

Route::post('/', DuplicateController::class . '@duplicate');
